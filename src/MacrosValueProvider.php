<?php

namespace HomeMe\MacrosTool;

/**
 * Провайдер значения макроса
 */
interface MacrosValueProvider
{
    /**
     * @param Macros $macros
     * @return string
     */
    public function getValue(Macros $macros);

    /**
     * @param Macros $macros
     * @return bool
     */
    public function isSupportedMacros(Macros $macros);
}