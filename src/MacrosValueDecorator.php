<?php

namespace HomeMe\MacrosTool;

/**
 * Декоратор значения макроса
 */
interface MacrosValueDecorator
{
    /**
     * @param Macros $macros
     * @param string $providerReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $providerReturnedValue);
}