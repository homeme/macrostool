<?php

namespace HomeMe\MacrosTool;

use HomeMe\MacrosTool\MacrosParser\Token;

final class Macros
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var array
     */
    private $attributes;

    /**
     * @param string $name
     * @param array $attributes
     */
    public function __construct($name, array $attributes)
    {
        $this->name = (string)$name;
        $this->attributes = $attributes;
    }

    /**
     * @param Token $token
     * @return Macros
     */
    public static function createFromToken(Token $token) {
        if (!$token->isMacros()) {
            throw new \LogicException('Token type must be macros');
        }

        return new self($token->getName(), $token->getAttributes());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @return mixed|null
     */
    public function getAttribute($name, $default = null) {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : $default;
    }
}