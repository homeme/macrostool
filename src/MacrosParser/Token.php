<?php

namespace HomeMe\MacrosTool\MacrosParser;

final class Token
{
    const TYPE_TEXT = 1;
    const TYPE_MACROS = 2;

    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $type;
    /**
     * @var string
     */
    private $value;
    /**
     * @var array
     */
    private $attributes;

    /**
     * @param int $type
     * @param string $name
     * @param string $value
     * @param array $attributes
     */
    private function __construct($type, $name, $value, array $attributes)
    {
        $this->name = (string)$name;
        $this->type = (int)$type;
        $this->value = (string)$value;
        $this->attributes = $attributes;
    }

    /**
     * @param string $value
     * @return Token
     */
    public static function createText($value) {
        return new self(self::TYPE_TEXT, '', $value, []);
    }

    /**
     * @param string $name
     * @param array $attributes
     * @return Token
     */
    public static function createMacros($name, array $attributes) {
        return new self(self::TYPE_MACROS, $name, null, $attributes);
    }

    /**
     * @return bool
     */
    public function isMacros() {
        return $this->type === self::TYPE_MACROS;
    }

    /**
     * @return bool
     */
    public function isText() {
        return $this->type === self::TYPE_TEXT;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}