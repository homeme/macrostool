<?php

namespace HomeMe\MacrosTool\MacrosParser;

final class TokenizedString
{
    /**
     * @var Token[]
     */
    private $tokens = [];

    /**
     * @param Token[] $tokens
     */
    public function __construct(array $tokens)
    {
        foreach ($tokens as $token) {
            $this->addToken($token);
        }
    }

    /**
     * @param Token $token
     * @return void
     */
    private function addToken(Token $token) {
        $this->tokens[] = $token;
    }

    /**
     * @return Token[]
     */
    public function getTokens()
    {
        return $this->tokens;
    }
}