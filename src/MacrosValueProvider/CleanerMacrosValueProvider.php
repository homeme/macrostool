<?php

namespace HomeMe\MacrosTool\MacrosValueProvider;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueProvider;

final class CleanerMacrosValueProvider implements MacrosValueProvider
{
    /**
     * @param Macros $macros
     * @return string
     */
    public function getValue(Macros $macros)
    {
        return '';
    }

    /**
     * @param Macros $macros
     * @return bool
     */
    public function isSupportedMacros(Macros $macros)
    {
        return true;
    }
}