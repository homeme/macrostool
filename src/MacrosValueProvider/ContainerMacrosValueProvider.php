<?php

namespace HomeMe\MacrosTool\MacrosValueProvider;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueProvider;

final class ContainerMacrosValueProvider implements MacrosValueProvider
{
    /**
     * @var MacrosValueProvider[]
     */
    private $providers = [];

    /**
     * @param MacrosValueProvider[] $providers
     */
    public function __construct(array $providers)
    {
        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
    }

    /**
     * @param MacrosValueProvider $provider
     * @return void
     */
    private function addProvider(MacrosValueProvider $provider) {
        $this->providers[] = $provider;
    }

    /**
     * @param Macros $macros
     * @return string
     */
    public function getValue(Macros $macros)
    {
        foreach ($this->providers as $provider) {
            if ($provider->isSupportedMacros($macros)) {
                return $provider->getValue($macros);
            }
        }

        throw new \InvalidArgumentException('Unsupported macros passed');
    }

    /**
     * @param Macros $macros
     * @return bool
     */
    public function isSupportedMacros(Macros $macros)
    {
        foreach ($this->providers as $provider) {
            if ($provider->isSupportedMacros($macros)) {
                return true;
            }
        }

        return false;
    }
}