<?php

namespace HomeMe\MacrosTool;

use HomeMe\MacrosTool\MacrosParser\Token;
use HomeMe\MacrosTool\MacrosParser\TokenizedString;

/**
 * Парсер макросов в тексте
 *
 * Пример строки с макросом:
 * "Скушай еще этих {{productName case="rod"}} и выпей {{productName case="dat"}}"
 */
final class MacrosParser
{
    /**
     * @var string
     */
    private $macrosBegin = '{{';

    /**
     * @var string
     */
    private $macrosEnd = '}}';

    /**
     * Разбить строку на токены
     *
     * @param string $text
     * @throws \InvalidArgumentException
     * @return TokenizedString
     */
    public function tokenize($text) {
        if (strpos($text, $this->macrosBegin) === false || strpos($text, $this->macrosEnd) === false) {
            return new TokenizedString([Token::createText($text)]);
        }

        $textTokenStart = '<text><![CDATA[';
        $textTokenEnd = ']]></text>';

        $text = $this->hideTags($text);


        // convert macros to tags and wrap text to tags
        $text = str_replace($this->macrosBegin, $textTokenEnd . '<', $text);
        $text = str_replace($this->macrosEnd, '/>' . $textTokenStart, $text);
        $text = $textTokenStart . $text . $textTokenEnd;

        try {
            $xml = new \SimpleXMLElement('<root>' . $text . '</root>');
        } catch (\Exception $e) {
            throw new \InvalidArgumentException('Invalid macros syntax in text', 0, $e);
        }

        $tokens = [];

        /**
         * @var string $name
         * @var \SimpleXMLElement $child
         */
        foreach ($xml->children() as $name => $child) {
            if ($name === 'text') {
                $tokens[] = Token::createText($this->restoreTags((string)$child));
            } else {
                $attributes = [];

                foreach ($child->attributes() as $key => $attribute) {
                    $attributes[$key] = $this->restoreTags((string)$attribute);
                }

                $tokens[] = Token::createMacros($name, $attributes);
            }
        }

        return new TokenizedString($tokens);
    }

    /**
     * Склеить токены в строку
     *
     * @param TokenizedString $tokenizedString
     * @return string
     */
    public function toString(TokenizedString $tokenizedString) {
        $text = '';

        foreach ($tokenizedString->getTokens() as $token) {
            if ($token->isMacros()) {
                $text .= $this->macrosBegin . $token->getName();

                if ($token->getAttributes()) {
                    $text .= ' ';

                    foreach ($token->getAttributes() as $key => $value) {
                        $text .= $key . '="' . $value . '"';
                    }
                }

                $text .= $this->macrosEnd;
            } else {
                $text .= $token->getValue();
            }
        }

        return $text;
    }

    /**
     * Replace exist tags to something other
     *
     * @param string $text
     * @return string
     */
    private function hideTags($text) {
        $text = str_replace('<', '[[[[%', $text);
        $text = str_replace('>', '%]]]]', $text);

        return $text;
    }

    /**
     * Restore tags
     *
     * @param string $text
     * @return string
     */
    private function restoreTags($text) {
        $text = str_replace('[[[[%', '<', $text);
        $text = str_replace('%]]]]', '>', $text);

        return $text;
    }
}