<?php

namespace HomeMe\MacrosTool;

use HomeMe\MacrosTool\MacrosParser\Token;
use HomeMe\MacrosTool\MacrosParser\TokenizedString;

/**
 * Замена макросов в тексте на значение
 */
final class MacrosReplacer
{
    /**
     * @var MacrosParser
     */
    private $macrosParser;
    /**
     * @var MacrosValueProvider
     */
    private $macrosValueProvider;
    /**
     * @var MacrosValueDecorator
     */
    private $macrosValueDecorator;

    /**
     * @param MacrosParser $macrosParser
     * @param MacrosValueProvider $macrosValueProvider
     * @param MacrosValueDecorator $macrosValueDecorator
     */
    public function __construct(
        MacrosParser $macrosParser,
        MacrosValueProvider $macrosValueProvider,
        MacrosValueDecorator $macrosValueDecorator
    ) {
        $this->macrosParser = $macrosParser;
        $this->macrosValueProvider = $macrosValueProvider;
        $this->macrosValueDecorator = $macrosValueDecorator;
    }

    /**
     * @param string $text
     * @throws \InvalidArgumentException
     * @return string
     */
    public function replace($text) {
        if (!$text) {
            return $text;
        }

        $tokenized = $this->macrosParser->tokenize($text);

        $newTokens = [];

        foreach ($tokenized->getTokens() as $token) {
            if ($token->isMacros()) {
                $macros = Macros::createFromToken($token);

                if ($this->macrosValueProvider->isSupportedMacros($macros)) {
                    $value = $this->macrosValueProvider->getValue($macros);
                    $value = $this->macrosValueDecorator->decorate($macros, $value);
                    
                    $token = Token::createText($value);
                }
            }

            $newTokens[] = $token;
        }

        return $this->macrosParser->toString(new TokenizedString($newTokens));
    }
}