<?php

namespace HomeMe\MacrosTool\MacrosValueDecorator;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueDecorator;

final class TextTransformValueDecorator implements MacrosValueDecorator
{
    /**
     * @param Macros $macros
     * @param string $providerReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $providerReturnedValue)
    {
        if (!$providerReturnedValue || !$macros->getAttribute('transform')) {
            return $providerReturnedValue;
        }

        switch ($macros->getAttribute('transform')) {
            case 'upper':
                return mb_strtoupper($providerReturnedValue);
            case 'lower':
                return mb_strtolower($providerReturnedValue);
            case 'title':
                return mb_convert_case($providerReturnedValue, MB_CASE_TITLE);
            case 'ucfirst':
                return mb_strtoupper(mb_substr($providerReturnedValue, 0, 1)) . mb_substr($providerReturnedValue, 1);
        }

        return $providerReturnedValue;
    }
}