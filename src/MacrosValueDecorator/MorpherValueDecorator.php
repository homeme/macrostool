<?php

namespace HomeMe\MacrosTool\MacrosValueDecorator;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueDecorator;
use HomeMe\Morpher\Morpher;

/**
 * Склонение по падежам значений макроса
 */
final class MorpherValueDecorator implements MacrosValueDecorator
{
    /**
     * @var Morpher
     */
    private $morpher;

    /**
     * @param Morpher $morpher
     */
    public function __construct(Morpher $morpher)
    {
        $this->morpher = $morpher;
    }

    /**
     * @param Macros $macros
     * @param string $providerReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $providerReturnedValue)
    {
        if ($providerReturnedValue && $macros->getAttribute('case')) {
            $providerReturnedValue = $this->morpher->inflect(
                $providerReturnedValue,
                $macros->getAttribute('case')
            );
        }

        return $providerReturnedValue;
    }
}