<?php

namespace HomeMe\MacrosTool\MacrosValueDecorator;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueDecorator;

final class NumberFormatValueDecorator implements MacrosValueDecorator
{
    /**
     * @param Macros $macros
     * @param string $providerReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $providerReturnedValue)
    {
        if ($providerReturnedValue === '') {
            return $providerReturnedValue;
        }

        if ($macros->getAttribute('numFormat')) {
            $providerReturnedValue = number_format(
                (float)$providerReturnedValue,
                (int)$macros->getAttribute('decimals', 0),
                $macros->getAttribute('decPoint', '.'),
                $macros->getAttribute('thousandsSep', ' ')
            );
        }

        return $providerReturnedValue;
    }
}