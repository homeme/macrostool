<?php

namespace HomeMe\MacrosTool\MacrosValueDecorator;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueDecorator;

final class DefaultValueDecorator implements MacrosValueDecorator
{
    /**
     * @param Macros $macros
     * @param string $providerReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $providerReturnedValue)
    {
        if ($providerReturnedValue === '' && $macros->getAttribute('default')) {
            return $macros->getAttribute('default');
        }

        return $providerReturnedValue;
    }
}