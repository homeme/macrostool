<?php

namespace HomeMe\MacrosTool\MacrosValueDecorator;

use HomeMe\MacrosTool\Macros;
use HomeMe\MacrosTool\MacrosValueDecorator;

final class ContainerMacrosValueDecorator implements MacrosValueDecorator
{
    /**
     * @var MacrosValueDecorator[]
     */
    private $decorators = [];

    /**
     * @param MacrosValueDecorator[] $decorators
     */
    public function __construct(array $decorators)
    {
        foreach ($decorators as $decorator) {
            $this->addDecorator($decorator);
        }
    }

    /**
     * @param MacrosValueDecorator $decorator
     * @return void
     */
    private function addDecorator(MacrosValueDecorator $decorator) {
        $this->decorators[] = $decorator;
    }

    /**
     * @param Macros $macros
     * @param string $decoratorReturnedValue
     * @return string
     */
    public function decorate(Macros $macros, $decoratorReturnedValue)
    {
        foreach ($this->decorators as $decorator) {
            $decoratorReturnedValue = $decorator->decorate($macros, $decoratorReturnedValue);
        }

        return $decoratorReturnedValue;
    }
}